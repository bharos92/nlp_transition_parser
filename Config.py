UNKNOWN = "UNK"
ROOT = "ROOT"
NULL = "NULL"
NONEXIST = -1

# max_iter = 1001
max_iter = 201
# batch_size = 
batch_size = 100
hidden_size = 200
embedding_size = 128
seed = 128
learning_rate = 0.1
display_step = 50
validation_step = 40
n_Tokens = 48
lam = 1e-8
